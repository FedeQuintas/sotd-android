package com.sotd.songs.actions

import com.sotd.songs.sotd.domain.Song

class CreateSong {

    fun create(title: String, tags: List<String>): Song {
        return Song(title, tags)
    }

}
