package com.sotd.songs.sotd.domain

class Song(title: String, tags: List<String>) {
    val tags = tags
    val title = title
}
