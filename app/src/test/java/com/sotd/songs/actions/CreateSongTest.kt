package com.sotd.test.songs.actions

import com.sotd.songs.sotd.actions.CreateSong
import org.junit.Test

import org.junit.Assert.*


class CreateSongTest {

    @Test
    fun songIsCreatedWithTitle() {

        val tags = listOf(Classical, Summer)

        var song = CreateSong().create(title, tags)

        assertEquals(song.title, title)
        assertEquals(song.tags, tags)
    }

    @Test
    fun songIsSentToApiGateway() {

        //val mockBookService = Mockito.mock(SongApiGateway::class.java)
        val tags = listOf(Classical, Summer)

        var song = CreateSong().create(title, tags)

        assertEquals(song.title, title)
        assertEquals(song.tags, tags)
    }

    companion object {
        const val title = "Title"
        const val Classical = "Classical"
        const val Summer = "Summer"
    }
}
